### This repo is originally forked from https://pypi.python.org/pypi/trello ###

New module added is "upload files as an attachement directly to trello card."

You can find documentation for the Python API at:
	http://packages.python.org/trello/

And documentation for the Trello API at:
	https://trello.com/docs/api/


### Setup
	clone/download repo and extract.
	$ cd zy-trello	
	$ python setup.py install
        
        or 

        $ pip install https://bitbucket.org/ashish2py/zy-trello/get/12a994cf962e.zip

### Quick start

	from trello import TrelloApi

	api_key = 'your-app-api-key'
	secret_key = 'secret-key'

	trello = TrelloApi(api_key)

	#trello api authentication to get ge user_auth_token
	trello.get_token_url('Notification', expires='never', write_access=True)

	api_access_token = 'access_token'

	#set api_access_token which helps to authenticate the request.
	trello.set_token(api_access_token)

	file_path = '/<file_path>/filename.txt'

	#file attachment to trello card.
	trello.cards.new_attachment_as_file('trello_card_id',file_path)


#for usage and implementation, refer 
zy_trello_config.ini and zy_trello_notification_api.py