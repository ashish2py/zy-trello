#!/usr/bin/env python
'''
AUTHOR : ASHISH T, http://www.ashishtiwari.me/
FOR DJANGO PROJECTS ONLY .

GENERATE REPORT AND SENDS EMAIL TO SUBSCRIBER

'''
import logging
# create logger
logger = logging.getLogger("logging_tryout2")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s;  %(levelname)s;  %(message)s","%Y-%m-%d %H:%M:%S")
ch.setFormatter(formatter)
logger.addHandler(ch)

import smtplib, os
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders

import csv
import operator
import tarfile

from django.conf import settings
from django.core.mail import EmailMessage
#EXTERNAL LIBRARY

import logging
import threading
from datetime import date
import xhtml2pdf.pisa as pisa

from datetime import date,timedelta,datetime
import time
import os
import glob
import fnmatch
import json
import cStringIO as StringIO
import psycopg2,psycopg2.extras
from psycopg2.extensions import adapt, register_adapter, AsIs

#TRELLO 
import argparse
import requests
import urllib2


import xlrd
import json
import sys
import os

import ConfigParser
config = ConfigParser.RawConfigParser()
config.read('notification_import.ini')

sys.path.append( '.' )
sys.path.append( '..' )
sys.path.append( '../../' )
sys.path.append( '../../home/')

your_settings_module = "core.settings"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", your_settings_module)

from django.conf import settings
from django.template import Context, Template

from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.template.defaulttags import register as template_register
from django.shortcuts import render_to_response
from django.template.loader import get_template

api_hook_alias = ''
practice_api=''
assessment_api=''

@template_register.filter
def get_item(dictionary, key):
	return dictionary.get(key, None)

@template_register.filter
def get_assessment_item(dictionary, key):
	return dictionary.get(str(key), None)

@template_register.filter
def get_avg(value, total):
	avg_cal=0
	try:
		avg_cal = float("{0:.2f}".format((value/total)*100))
		return avg_cal
	except Exception as e:
		return avg_cal

subscriber_list = {}
school_name=''
past_day = ''
db_alias = ''

def main_thread(database,*args):
	global api_hook_alias
	global school_name
	global past_day
	global db_alias
	
	school_name=database
	db_name=database
	db_alias=config.get('db_map',db_name)

	logger.info('#####################################')
	logger.info('NOTIFICATION MANAGER WELCOMES YOU!!')
	logger.info('#####################################')
	logger.info('Connected database : '+db_name)

	#notifiy developer
	report_process_notify_mail = EmailSender()
	report_process_notify_mail.processing_email()

	try:
		if config.get('report_generate_date',db_name)==get_current_date():
			logger.info('Request has been already processed. Shutting down the process.')
			sys.exit()
		else:
			past_day = config.get('report_generate_date',school_name)+" 00:00:00.000000"
			logger.info('Report generating from : '+str(past_day))
	except Exception as e:
		logger.error('Oops, No last sync date for '+db_name+'. Dont worry, we will do this for you.')
		config.set('report_generate_date', db_name, get_current_date())
		past_day = datetime.now() - timedelta(days=7)
		logger.info('Report generating from : '+str(past_day))
		config.write(open('notification_import.ini',"w"))
		pass

	try:
		api_hook_alias='?alias='+config.get('db_map',db_name)
		logger.info('alias : '+api_hook_alias)
	except Exception as e:
		logger.exception('DB mapping does not exist')

	#########################
	##	Practice processing
	#########################
	practice = PracticeProcessing(db_name)
	fileuploader = FileUploader(config.get('request_type','practice'))	
	
	###########################
	##	Assessment processing
	###########################
	assessment = AssessmentProcessing(db_name)	
	fileuploader = FileUploader(config.get('request_type','assessment'))	
	
	logger.info('### Subscriber list processing. ###')	
	api_hook_processor = ApiHookProcessor(config.get('request_type','subscriber'),None)
	
	logger.info('subscriber list generated.')	
	logger.info('### Email sending ###')
	email_sender = EmailSender(subscriber_list)
	
	#loggin last sync date
	logger.info('Logging sync date of database')
	config.set('report_generate_date', db_name, get_current_date())	
	config.write(open('notification_import.ini',"w"))
	
	logger.info('sync database date setup to : '+config.get('report_generate_date', db_name))
	#weekly_email = WeeklyFileProcessing()
	#make_tarfile()
	
	logger.info('Closing appliaction..')
	logger.info('######################################')
	logger.info('THANKS FOR USING NOTIFICATION MANAGER')
	logger.info('######################################')


class NotificationManager:	
	def __init__(self, db_name):
		self.db_name=db_name

class PracticeProcessing:
	def __init__(self,db_name):
		self.db=db_name
		self.practice_record_list=[]
		self.lesson_list = set()
		#calling functions
		self.processing_practice()
		self.practice_report_generator()
	
	def processing_practice(self):
		logger.info('### PRACTICE REPORT PROCESSING ###')
		
		database_connection = psycopg2.connect(database=self.db, user="postgres", password="zayalearn", host="localhost", port="5432")
		db_cursor = database_connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
		
		logger.info('database connected # '+str(self.db))
		db_cursor.execute("SELECT * FROM practice_practice where mtime >= %s",(past_day,))
		records = db_cursor.fetchall()         #fetchall records   
		practice_report=records
		logger.info('processing practice queryset.')
		for practice in practice_report:
			try:
				db_cursor.execute("SELECT * FROM content_lesson where uuid = %s",(practice['app'],))
				lesson_record = db_cursor.fetchone()
				lesson_id = lesson_record['id']				
				report_info = {
					'class_id':'',
					'lesson_id':'',
					'lesson_name':''
				}
				if lesson_id not in self.lesson_list:
					db_cursor.execute("select * from local_student where account_ptr_id = %s",(practice['account_id'],))
					class_queryset = db_cursor.fetchone()
					self.lesson_list.add(lesson_record['id'])
					report_info['lesson_id'] = lesson_record['id']
					report_info['lesson_name']= lesson_record['name']
					report_info['class_id']=class_queryset['std_class_id']
					self.practice_record_list.append(report_info)  								#adding records to list
			except Exception as e:
			   pass
		logger.info('practice records generated #'+str(len(self.practice_record_list)))
		logger.info('practice query done.')
		db_cursor.close()
	
	def practice_report_generator(self):
		logger.info('practice report genration started.')
		api_hook_response = ApiHookProcessor(config.get('request_type','practice'),self.practice_record_list)
	
	def practice_report_status(self):
		print 'practice report status'
	
	def status_update(self):
		print 'practice report status update final'
	
class AssessmentProcessing:
	def __init__(self, db_name):
		self.db=db_name
		self.assessment_record_list=[]
		self.assessment_list=set()
		self.processing_assessment()
		self.assessment_report_generator()
		
	def processing_assessment(self):
		logger.info('### ASSESSMENT REPORT PROCESSING ###')
		
		database_connection = psycopg2.connect(database=self.db, user="postgres", password="zayalearn", host="localhost", port="5432")
		db_cursor = database_connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
		
		logger.info('database connected # '+str(self.db))
		
		db_cursor.execute("SELECT * FROM assessment_report where finish_time >= %s",(past_day,))
		records = db_cursor.fetchall()         #fetchall records   
		assessment_report=records
			
		logger.info('processing assessment queryset.')
		for assessment in assessment_report:
			try:
				assessment_id = assessment['assessment_id']
				if assessment_id not in self.assessment_list:
					report_info = {
						'class_id':'',
						'assessment_id':'',
					}
			
					self.assessment_list.add(assessment_id)
					db_cursor.execute("""select * from local_student where account_ptr_id = %s""",(assessment['student_id'],))
					student_record = db_cursor.fetchone()
					student_class = student_record['std_class_id']
					report_info['class_id']=student_class
					report_info['assessment_id']=assessment_id
					self.assessment_record_list.append(report_info)
			except Exception as e:
			   logger.exception(e)
		logger.info('assessment query done.')
		logger.info('assessment records generated #'+str(len(self.assessment_record_list)))
		db_cursor.close()
	
	def assessment_report_generator(self):
		logger.info('assessnet report genration started.')
		api_hook_response = ApiHookProcessor(config.get('request_type','assessment'),self.assessment_record_list)
	
	def assessment_report_status(self, files):
		pass

class ApiHookProcessor:
	'''
	API calls for practice, assessment and notifiction subscriber.
	'''
	def __init__(self,process_type,obj_list):
		self.request_type=process_type
		self.list=obj_list
		self.practice_context=[]
		self.assessment_context=[]

		#processing
		self.request_processing(self.request_type)
		logger.info('passing context to generate pdf')
		
		if self.request_type==config.get('request_type','practice'):
			logger.info('processing practice context.')
			file_handling = FileHanding(self.practice_context,self.request_type)
			
		if self.request_type==config.get('request_type','assessment'):
			logger.info('processing assessment context.')
			file_handling = FileHanding(self.assessment_context,self.request_type)
		
	def request_processing(self,process_type):
		if process_type==config.get('request_type','practice'):
			practice_list = self.list
			
			for params in practice_list:
				class_id = params['class_id'] if params['class_id'] else 0
				lesson_id = params['lesson_id'] if params['lesson_id'] else 0
				try:
					api_hook = '%s/practice_report/class/%s/lesson/%s/%s'%(config.get('api_hooks','host_name'),params['class_id'],params['lesson_id'],api_hook_alias)				   
					logger.info('START practice api calls.'+api_hook)			
					response = urllib2.urlopen(api_hook)
					context = response.read()
					self.practice_context.append(json.loads(context))
					logger.info('DONE practice api and context created.')			
				except urllib2.HTTPError, e:
					logger.exception(e.code)
				except urllib2.URLError, e:
					logger.exception(e.args)

		if self.request_type==config.get('request_type','assessment'):
			assessment_list = self.list
			
			for params in assessment_list:
				class_id = params['class_id'] if params['class_id'] else 0
				assessment_id = params['assessment_id'] if params['assessment_id'] else 0
				try:
					api_hook = '%s/api/v1/summary/%s/%s/%s'%(config.get('api_hooks','host_name'),params['class_id'],params['assessment_id'],api_hook_alias)
					logger.info('START assessment api calls.'+api_hook)			
					response = urllib2.urlopen(api_hook)
					context = response.read()
					self.assessment_context.append(json.loads(context))
					logger.info('DONE assessment api and context created.')			
				except urllib2.HTTPError, e:
					logger.exception(e.code)
				except urllib2.URLError, e:
					logger.exception(e.args)

		if self.request_type==config.get('request_type','subscriber'):
			global subscriber_list
			logger.info('getting subscriber list.')
			try:
			   api_hook = config.get('api_hooks','host_name')+config.get('api_hooks','subscriber_api')+api_hook_alias
			   logger.info('calling subscriber api @'+api_hook)
			   response = urllib2.urlopen(api_hook)
			   context = response.read()
			   context = json.loads(context)			   
			   subscriber_list = context['objects']			   
			except urllib2.HTTPError, e:
				logger.exception(e.code)
			except urllib2.URLError, e:
				logger.exception(e.args)

class FileHanding:
	'''
	Processing context and creating files.
	1. create report directory
	2. create pdf report on the basis of report type[assessment/practice]
	'''
	 
	def __init__(self,context_list,process_type):
		self.context_list = context_list
		self.process_type = process_type		
		self.current_date = (datetime.now()).strftime("%d-%m-%y")
		
		self.dir_creator()
		self.pdf_generator()		
		
	def pdf_generator(self):
		logger.info('creating PDF reports.')
		logger.info('pdf report to be generated #'+str(len(self.context_list)))
		file_path=config.get('path','report_location')+school_name+'/'
		with open(file_path+config.get('report_csv',self.process_type)+get_current_date()+'.csv','w') as csvfile:
			logger.info('Writing information to CSV file.')
			fieldnames = ['file_type','filepath','filename']
			writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
			writer.writeheader()
			
			data={
				'file_type':'',
				'filepath':'',
				'filename':'',
			}
		
			for context in self.context_list:
				template=None
				if self.process_type==config.get('request_type','practice'):
					logger.info('Generating report for #'+str(context['class']['name']))
					logger.info('writing practice report')
					filename = self.file_naming(context['class']['name'],context['lesson']['name'])
					template = get_template('practice_report_pdf.html')
					self.pisa_generator(context,template,filename)
					data['file_type']=config.get('request_type','practice')
					data['filepath']=filename
					data['filename']=filename.split('/')[-1]
					
				if self.process_type==config.get('request_type','assessment'):
					logger.info('writing assessment report')
					filename = self.file_naming(context['class']['name'],context['assessment'])
					template = get_template('notification_assessment_report_pdf.html')
					self.pisa_generator(context,template,filename)
					data['file_type']=config.get('request_type','assessment')
					data['filepath']=filename
					data['filename']=filename.split('/')[-1]
				writer.writerow(data)
			logger.info('Writing information to CSV file done.')

	def pisa_generator(self,context,template,filename):
		logger.info('pdf report generating #'+filename)
		report_context = Context(context)
		html  = template.render(report_context)
		result = open(filename, 'wb')
		pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result)
		result.close()
		logger.info('pdf report generated.')
		
	def file_naming(self,class_name,report_name):
		filename = ''			
		logger.info('file naming....')
		current_date = get_current_date()
		if self.process_type==config.get('request_type','practice'):        
			filename = config.get('path','report_location')+school_name+'/'+config.get('path','practice')+self.process_type+'__'+class_name+'__'+report_name.replace('/','#')+'__'+current_date+'.pdf'
			logger.info('file naming done @'+filename)
		elif self.process_type==config.get('request_type','assessment'):
			filename = config.get('path','report_location')+school_name+'/'+config.get('path','assessment')+self.process_type+'__'+class_name+'__'+report_name.replace('/','#')+'__'+current_date+'.pdf'
			logger.info('file naming done @'+filename)
		return filename
	
	def dir_creator(self):
		logger.info('checking for report directory.')
		logger.info('request to generate '+self.process_type)
		dir_location = config.get('path','report_location')
		
		if self.process_type==config.get('request_type','practice'):
			if not os.path.exists(dir_location+'/'+school_name+'/practice'):
				logger.info('report directory created :@'+dir_location+school_name+'/practice')		
				os.makedirs(dir_location+'/'+school_name+'/practice')
		
		if self.process_type==config.get('request_type','assessment'):
			if not os.path.exists(dir_location+'/'+school_name+'/assessment'):
				logger.info('report directory created :@'+dir_location+school_name+'/assessment')		
				os.makedirs(dir_location+'/'+school_name+'/assessment')

class FileUploader:
	def __init__(self,request_type):
		self.request_type=request_type
		report_handler = self.report_handler()
		
	def practice_file_handler(self):
		pass
		
	def assessment_file_handler(self):
		pass
		
	def report_handler(self):		
		logger.info('Exporting report file.')
		report_full_path = config.get('path','report_location')+school_name+'/'
		practice_report_csv=report_full_path+config.get('report_csv','practice')+get_current_date()+'.csv'
		assessment_report_csv=report_full_path+config.get('report_csv','assessment')+get_current_date()+'.csv'

		#creating list and card for new object.
		if 'assessment' == self.request_type:
			self.trello_api_request(assessment_report_csv)

		if 'practice' == self.request_type:
			self.trello_api_request(practice_report_csv)			
		
		logger.info('All uploading done.')


	def trello_api_request(self,file_name):
		trelloAPI = TrelloAPIHandler()		
		status = {
			'filename':'',
			'url':'',
			'status':''
		}

		trello_report_status_f = config.get('path','report_location')+school_name+'/'+config.get('report_csv','trello_report')+get_current_date()+'.csv'
		try:
			logger.info('reading file : '+file_name)
			with open(file_name,'rb') as report_f, open(trello_report_status_f,'a') as trello_status_f:
				#writing files to trello report.
				fieldnames = ['filename', 'filepath','url','status']
				writer = csv.DictWriter(trello_status_f, fieldnames=fieldnames)

				rdr = csv.DictReader(report_f,delimiter=',')
				sortedlist = sorted(rdr, key=operator.itemgetter('filepath'), reverse=True)
				
				header_write = False

				if self.request_type == 'practice' and header_write==False:
					header_write==True
					writer.writeheader()

				for row in sortedlist:
					#avoid writing header on each attempt.
					trello_response = trelloAPI.processing_files(row['filepath'])
					status['filepath']=row['filepath']
					status['filename']=trello_response['name']
					status['url']=trello_response['url']
					status['status']='done'
					writer.writerow(status)

		except Exception as e:
			pass
			logger.exception(e)
	
class TrelloAPIHandler:

	def __init__(self):
		self.path = ''
		self.trello_board = ''
		self.trello_board_id=''
		self.trello_board_list={}
		self.trello_board_list_complete=[]
		self.trello_api_call_status=True
		self.file_path=''
		#create_list = self.create_list(self.path)
		
	def processing_files(self,file_path):
		self.file_path=file_path
		self.trello_board = config.get('db_map',school_name)
		self.trello_board_id = config.get('trello_board',self.trello_board)
		self.current_card_list=[]

		if self.trello_api_call_status:

			try:
				from trello import TrelloApi
				trello_call = TrelloApi(config.get('trello_cred','api_key'))
				trello_call.set_token(config.get('trello_cred','api_access_token'))

				last_grade_processed=None
				logger.info('checking available list on board.')
				t_b_list = trello_call.boards.get_list(self.trello_board_id)

				#get the list of trello card.
				for item in t_b_list:
					self.trello_board_list[str(item['name'])]=item['id']

				#check if the list is not created then create the list and add cards.
				grade=get_grade(self.file_path)

				if grade not in self.trello_board_list.keys():
					logger.info('creating list for # '+grade)
					list_res = trello_call.boards.new_list(self.trello_board_id,grade)
					card = trello_call.lists.new_card(list_res['id'],get_lesson_assess_name(self.file_path))
					card_res=trello_call.cards.new_attachment_as_file(card['id'],self.file_path)
					logger.info('attachment added successfully')
					self.trello_board_list={}
					return card_res

				elif grade in self.trello_board_list.keys():
					logger.info('list already exist')
					list_id=self.trello_board_list[grade]
					card_list = trello_call.lists.get_card(list_id)

					card_names = {}
					for card_item in card_list:
						card_names[card_item['name']]=card_item['id']

					lesson_assess_name = get_lesson_assess_name(self.file_path)

					if lesson_assess_name in card_names.keys():
						logger.info('card alrady exist #'+lesson_assess_name)
						card_res = trello_call.cards.new_attachment_as_file(card_names[lesson_assess_name],self.file_path)
						logger.info('attachment added to '+lesson_assess_name)				
						return card_res

					if lesson_assess_name not in card_names.keys():
						logger.info('creating new cards #'+lesson_assess_name)
						card_id = trello_call.lists.new_card(list_id,lesson_assess_name)
						card_res = trello_call.cards.new_attachment_as_file(card_id['id'],self.file_path)
						logger.info('attachment added to '+lesson_assess_name)
						logger.info('')
						return card_res

					self.trello_board_list={}
			except Exception as e:
				pass
	
	def create_list(self):
		'''
		lists are grade based.
		'''
	
	def create_card(self):
		'''
		cards are lesson name and assessment based.
		'''
		pass

class EmailSender:
	def __init__(self,subscriber_list=None):
		self.subscriber_list=subscriber_list
		self.attachment_report=config.get('path','report_location')+school_name+'/'+config.get('path','weekly_report')+get_current_date()+'.tar.gz'
		if subscriber_list:
			self.each_report_email()
		#self.weekly_email()
		#self.django_email_test()
	def each_report_email(self):
		server="localhost"
		try:			
			with open(config.get('path','report_location')+school_name+'/'+config.get('report_csv','trello_report')+get_current_date()+'.csv','rb') as trello_f:
				reader = csv.DictReader(trello_f)
				logger.info('Opening trello report status file to notify subscriber')
				for files in reader:
					for receiver in self.subscriber_list:
						logger.info('Sending emil to '+receiver['email_id'])
						mail_context = {
							'url':files['url'],
							'filename':files['filename'],
							'username':receiver['username'].title()
						}
						send_to = [receiver['email_id'],]
						send_from="notification@zaya.in"
						message = get_template('emailer_class_report.html').render(Context(mail_context))
						subject = "New report for '%s' school"%(school_name)
						msg = EmailMessage(subject, message, to=send_to, from_email=send_from)
						msg.content_subtype = 'html'
						msg.send()
						logger.info('Email successfully sent.')
		except Exception as e:
			print 'Exception found ',e

	def weekly_email(self):
		try:
			report_list = {'reports':self.mail_context()}
			send_to = [receiver['email_id'] for receiver in self.subscriber_list]
			send_from="notification@zaya.in"
			message = get_template('emailer_weekly.html').render(Context(report_list))
			subject = "New report for '%s' school"%(school_name)
			logger.info('Sending email.')
			msg = EmailMessage(subject, message, to=send_to, from_email=send_from)
			msg.attach_file(self.attachment_report)
			msg.content_subtype = 'html'
			msg.send()
			logger.info('Email successfully sent.')
		except Exception as e:
			print 'Exception found ',e

	def mail_context(self):
		with open(config.get('path','report_location')+school_name+'/'+config.get('report_csv','week_trello_report')+get_current_date()+".csv",'rb') as weekly_report_f:
			reader = csv.DictReader(weekly_report_f)
			logger.info('Opening trello report status file to notify subscriber')
			report_file_name = []
			report_list = []
			for report in reader:
				if report['filename'] not in report_file_name:
					mail_context = {'url':'','filename':''}
					report_file_name.append(report['filename'])
					mail_context['url']=report['url']
					mail_context['filename']=report['filename']
					report_list.append(mail_context)
			return report_list

	def processing_email(self):
		logger.info('Notifiying developer about processing.')
		send_to = ['ashish@zaya.in']
		send_from="notification@zaya.in"
		subject = "Report processing for %s"%(school_name)
		message = "Hi Team,<br/> New report procesing started for <b>%s</b> school.<br/><br/>Thank you."%(school_name)
		msg = EmailMessage(subject, message, to=send_to, from_email=send_from)
		msg.content_subtype = 'html'
		logger.info('notifying done.')
		msg.send()


def weekly_api_call():
	weekly_api = WeeklyFileProcessing()

class WeeklyFileProcessing:
	
	def __init__(self):
		self.school_db_list = dict(config.items('db_map')).keys()
		self.file_path = config.get('path','report_location')+school_name+'/'
		self.assessment_files_path = self.file_path+config.get('path','assessment')
		self.practice_files_path = config.get('path','practice')
		self.trello_report_file = config.get('report_csv','trello_report')
		self.week_days_list=[]
		self.file_processing()
		
	def file_processing(self):
		file_list = glob.glob(os.path.join(self.file_path,self.trello_report_file+'*.csv'))
		file_list.sort()	
		end_date = date.today() - timedelta(days=7)
		delta=timedelta(days=0)
		status = {'filename':'','url':'','status':''}
		for dt in week_dates(end_date, date.today()+timedelta(days=1), timedelta(days=1)):
			self.week_days_list.append(str(dt))
	
		with open(self.file_path+config.get('report_csv','week_trello_report')+get_current_date()+".csv",'wb') as weekly_report_f:
			fieldnames = ['filename', 'filepath','url','status']
			writer = csv.DictWriter(weekly_report_f, fieldnames=fieldnames)
			writer.writeheader()

			for file in file_list:
				if file.split('/')[-1].split('_')[-1].split('.')[0] not in self.week_days_list:
					continue
				with open(file,'rb') as process_file:	
					rdr = csv.DictReader(process_file)				
					for row in rdr:					
						status['filepath']=row['filepath']
						status['filename']=row['filename']
						status['url']=row['url']
						writer.writerow(status)	
def make_tarfile():
	logger.info('Making tar.gz file of report.')
	list_files = []
	destionation_path=config.get('path','report_location')+school_name+'/'+config.get('path','weekly_report')+get_current_date()
	file_path=config.get('path','report_location')+school_name+'/'
	with open(file_path+config.get('report_csv','week_trello_report')+get_current_date()+".csv",'rb') as weekly_report_f:
		rdr = csv.DictReader(weekly_report_f)
		for row in rdr:
			list_files.append(row['filepath'])
		
	"""
	This function creates tar for given list of files.
	list_path         : <List> Indicates the files to be included in tar.
	destionation_path : <String> Name and location where new tar file should be kept
	"""
	tar = tarfile.open(destionation_path+'.tar.gz', "w|gz")
	for file_path in list_files:
		file_name = file_path.split("/")[-1]
		tar.add(file_path, arcname = file_name)
	tar.close()
	logger.info('TAR file created.')

def week_dates(start, end, delta):
	curr = start
	while curr < end:
		yield curr
		curr += delta
		
#function calling
def get_current_date():
	current_date = date.today()
	return current_date.strftime('%Y-%m-%d')

def get_grade(name):
	return name.split('__')[1]

def get_lesson_assess_name(name):
	return name.split('__')[0].split('/')[-1]+' : '+name.split('__')[2]



#MAIN THREAD CALL
main_thread('<database-name>',None)